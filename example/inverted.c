static char help[] = "Create and base mesh with inverted corners\n\n";

#include <petscdm.h>
#include <petscdmplex.h>
#include <petscoptions.h>

static PetscErrorCode CreateBaseMesh(MPI_Comm comm, DM *baseMesh)
{
  /*
   * 11--12--13
   *  | 5 | 6 |
   *  7---8---9--10
   *  | 2 | 3 | 4 |
   *  3---4---5---6
   *      | 0 | 1 |
   *      0---1---2
   *
   */
  const PetscInt numCells = 7;
  const PetscInt numVerts = 14;
  double (vertCoords [])[2] =
    {           {1., 0.}, {2., 0.}, {3., 0.},
      {0., 1.}, {1., 1.}, {2., 1.}, {3., 1.},
      {0., 2.}, {1., 2.}, {2., 2.}, {3., 2.},
      {0., 3.}, {1., 3.}, {2., 3.} };

  /* PETSc DMPlex cell-to-numbering order
   *
   *  3 ---- 2
   *  |      |
   *  |      |
   *  0 ---- 1
   *
   */
  int (cellToVertex[])[4] =
    { { 0,  1,  5,  4}, { 1,  2,  6,  5},
      { 3,  4,  8,  7}, { 4,  5,  9,  8}, { 5,  6, 10,  9},
                        { 7,  8, 12, 11}, { 8,  9, 13, 12} };
  PetscMPIInt    rank;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = MPI_Comm_rank(comm, &rank);CHKERRQ(ierr);
  ierr = DMPlexCreateFromCellListParallelPetsc(
           comm,
           2, /* intrinsic dimension */
           rank ? 0    : numCells,
           rank ? 0    : numVerts,
           4, /* vertices per cell */
           PETSC_TRUE, /* "interpolate": create edges as mesh entities (points) */
           rank ? NULL : (const int *) &(cellToVertex[0][0]),
           2, /* embedded dimension */
           rank ? NULL : (const double *) &(vertCoords[0][0]),
           NULL,
           baseMesh);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode LabelBaseMesh(DM baseMesh)
{
  PetscInt lowerVertices[] = {0, 1, 2, 6, 10};
  PetscInt upperVertices[] = {3, 7, 11, 12, 13};
  PetscInt vStart, vEnd;
  const PetscInt vertsPerSide = 5;
  DMLabel dLabel = NULL;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMPlexGetDepthStratum(baseMesh, 0, &vStart, &vEnd);CHKERRQ(ierr);
  ierr = DMCreateLabel(baseMesh, "dirichlet");CHKERRQ(ierr);
  ierr = DMGetLabel(baseMesh, "dirichlet", &dLabel);CHKERRQ(ierr);
  for (PetscInt i = 0; i < 2; i++) {
    const PetscInt *verts = i ? (const PetscInt *) upperVertices : (const PetscInt *) lowerVertices;

    for (PetscInt j = 0; j < vertsPerSide; j++) {
      PetscInt vertA = vStart + verts[j];

      for (PetscInt k = j+1; k < vertsPerSide; k++) {
        PetscInt joinSize = 0;
        const PetscInt *join = NULL;
        PetscInt vertB = vStart + verts[k];
        PetscInt pair[2];

        pair[0] = vertA;
        pair[1] = vertB;

        ierr = DMPlexGetJoin(baseMesh, 2, pair, &joinSize, &join);CHKERRQ(ierr);
        if (joinSize > 0) {
          for (PetscInt l = 0; l < joinSize; l++) {
            ierr = DMLabelSetValue(dLabel, join[l], i);CHKERRQ(ierr);
          }
        }
        ierr = DMPlexRestoreJoin(baseMesh, 2, pair, &joinSize, &join);CHKERRQ(ierr);
      }
    }
  }
  ierr = DMPlexLabelComplete(baseMesh, dLabel);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode CreateComputationMesh(DM baseMesh, DM *compMesh)
{
  PetscBool      flg;
  const char   *(options []) = {"p4est", "triangle"};
  MPI_Comm       comm;
  PetscInt       value = -1;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  comm = PetscObjectComm((PetscObject)baseMesh);
  ierr = PetscOptionsBegin(comm, "", "Mesh conversion options", "DMPLEX");CHKERRQ(ierr);
  ierr = PetscOptionsEList("-convert", "Convert base mesh to different type", "inverted", (const char *const *) options, 2, options[0], &value, &flg);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();
  *compMesh = NULL;
  if (flg) {
    if (value == 0) {
      ierr = DMConvert(baseMesh,DMP4EST,compMesh);CHKERRQ(ierr);
    } else {
      ierr = DMPlexSetCellRefinerType(baseMesh, DM_REFINER_TO_SIMPLEX);CHKERRQ(ierr);
      ierr = DMRefine(baseMesh, comm, compMesh);CHKERRQ(ierr);
    }
  }
  if (!*compMesh) {
    *compMesh = baseMesh;
    ierr = PetscObjectReference((PetscObject) baseMesh);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

int main(int argc, char **argv)
{
  DM             baseMesh, compMesh;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL,help);if (ierr) return ierr;
  ierr = CreateBaseMesh(PETSC_COMM_WORLD, &baseMesh);CHKERRQ(ierr);
  ierr = LabelBaseMesh(baseMesh);CHKERRQ(ierr);
  ierr = CreateComputationMesh(baseMesh, &compMesh);CHKERRQ(ierr);
  ierr = DMDestroy(&baseMesh);CHKERRQ(ierr);
  ierr = DMSetFromOptions(compMesh);CHKERRQ(ierr);
  ierr = DMSetUp(compMesh);CHKERRQ(ierr);
  ierr = DMViewFromOptions(compMesh,NULL,"-dm_view");CHKERRQ(ierr);
  ierr = DMDestroy(&compMesh);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}
